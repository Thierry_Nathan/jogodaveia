﻿namespace Jogodaidosa
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxJogadorUm = new System.Windows.Forms.TextBox();
            this.textBoxJogadorDois = new System.Windows.Forms.TextBox();
            this.buttonJogar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player 1:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Player 2:";
            // 
            // textBoxJogadorUm
            // 
            this.textBoxJogadorUm.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJogadorUm.Location = new System.Drawing.Point(138, 33);
            this.textBoxJogadorUm.Name = "textBoxJogadorUm";
            this.textBoxJogadorUm.Size = new System.Drawing.Size(378, 34);
            this.textBoxJogadorUm.TabIndex = 2;
            this.textBoxJogadorUm.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxJogadorDois
            // 
            this.textBoxJogadorDois.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJogadorDois.Location = new System.Drawing.Point(138, 91);
            this.textBoxJogadorDois.Name = "textBoxJogadorDois";
            this.textBoxJogadorDois.Size = new System.Drawing.Size(378, 34);
            this.textBoxJogadorDois.TabIndex = 3;
            this.textBoxJogadorDois.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxJogadorDois_KeyPress);
            // 
            // buttonJogar
            // 
            this.buttonJogar.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonJogar.Location = new System.Drawing.Point(151, 145);
            this.buttonJogar.Name = "buttonJogar";
            this.buttonJogar.Size = new System.Drawing.Size(271, 51);
            this.buttonJogar.TabIndex = 4;
            this.buttonJogar.Text = "Jogar";
            this.buttonJogar.UseVisualStyleBackColor = true;
            this.buttonJogar.Click += new System.EventHandler(this.buttonJogar_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 198);
            this.Controls.Add(this.buttonJogar);
            this.Controls.Add(this.textBoxJogadorDois);
            this.Controls.Add(this.textBoxJogadorUm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(585, 237);
            this.MinimumSize = new System.Drawing.Size(585, 237);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JOGO DA VELHA HD 4K";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxJogadorUm;
        private System.Windows.Forms.TextBox textBoxJogadorDois;
        private System.Windows.Forms.Button buttonJogar;
    }
}