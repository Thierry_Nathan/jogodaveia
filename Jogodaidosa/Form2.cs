﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jogodaidosa
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonJogar_Click(object sender, EventArgs e)
        {
            if (textBoxJogadorUm.Text == "")
            {
                textBoxJogadorUm.Text = "Player 1";
            }
            if (textBoxJogadorDois.Text == "")
            {
                textBoxJogadorDois.Text = "Player 2";
            }
            Form1.definirNomeJogador(textBoxJogadorUm.Text, textBoxJogadorDois.Text);
            this.Close();
        }

        private void textBoxJogadorDois_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "\r")
            {
                buttonJogar.PerformClick();
            }
        }       
    }
}
