﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jogodaidosa
{
    public partial class Form1 : Form
    {
        bool vezDoX = true;
        int qtdTurnos = 0;
        static string nomeJogadorUm;
        static string nomeJogadorDois;
        bool contraOComputador = false;

        public static void definirNomeJogador(string nomeUm, string nomeDois)
        {
            nomeJogadorUm = nomeUm;
            nomeJogadorDois = nomeDois;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Criado por Thierry Nathan", "Sobre");
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Volte sempre ;)", "Sobre");
            Application.Exit();
        }

        private void buttonSimbol_Click(object sender, EventArgs e)
        {
            if (vezDoX == true)
            {
                 vezDoX = false;
                 label1.Text = "O";
                 label2.Text = "X";
            }
            else
            {
                 vezDoX = true;
                 label1.Text = "X";
                 label2.Text = "O";
            }
            
        }
        private void button_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button2.Enabled = false;

            if (vezDoX == true)
            {
                button.Text = "X";
            }
            else
            {
                button.Text = "O";
            }

            vezDoX = !vezDoX;
            qtdTurnos++;
            button.Enabled = false;

            VerificaVencedor();

            if ((!vezDoX) && (contraOComputador == true))
            {
                ComputadorFazJogada();
            }
        }

        private void VerificaVencedor()
        {
            bool houveVencedor = false;

            //horizontais
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text)
                && (A1.Enabled == false))
            {
                houveVencedor = true;
            }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text)
                && (B1.Enabled == false))
            {
                houveVencedor = true;
            }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text)
                && (C1.Enabled == false))
            {
                houveVencedor = true;
            }

            //verticais
            if ((A1.Text == B1.Text) && (B1.Text == C1.Text)
               && (A1.Enabled == false))
            {
                houveVencedor = true;
            }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text)
                && (A2.Enabled == false))
            {
                houveVencedor = true;
            }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text)
                && (A3.Enabled == false))
            {
                houveVencedor = true;
            }

            //diagonais
            if ((A1.Text == B2.Text) && (B2.Text == C3.Text)
               && (A1.Enabled == false))
            {
                houveVencedor = true;
            }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text)
                && (C1.Enabled == false))
            {
                houveVencedor = true;
            }
     
            //vencedor
            if (houveVencedor)
            {
                DesabilitaBotoes();
                string vencedor = "";
                if (vezDoX)
                {
                    vencedor = nomeJogadorDois;
                    vezDoX = false;
                    if (label2.Text == "O")
                    {
                        label8.Text = (int.Parse(label8.Text) + 1).ToString();
                    }
                    else
                    {
                        label6.Text = (int.Parse(label6.Text) + 1).ToString();
                    }
                }
                else
                {
                    vencedor = nomeJogadorUm;
                    vezDoX = true;
                    if (label2.Text == "X")
                    {
                        label8.Text = (int.Parse(label8.Text) + 1).ToString();
                    }
                    else
                    {
                        label6.Text = (int.Parse(label6.Text) + 1).ToString();
                    }
                }

                MessageBox.Show("O vencedor é " + vencedor + "!", "PARABÉNS!!");

                qtdTurnos = 0;
                foreach (Control controle in Controls)
                {
                    if (controle.GetType().Name == "Button")
                    { 
                        controle.Enabled = true;
                        controle.Text = "";
                        button2.Text = "APENAS EM UM NOVO JOGO";
                        button2.Enabled = false;
                    }
                }
            }
            else
            {
                if (qtdTurnos == 9)
                {
                    label7.Text = (int.Parse(label7.Text) + 1).ToString();
                    MessageBox.Show("DEU VELHA!", "Velha");

                    qtdTurnos = 0;
                    foreach (Control controle in Controls)
                    {
                        if (controle.GetType().Name == "Button")
                        {
                            controle.Enabled = true;
                            controle.Text = "";
                            button2.Text = "APENAS EM UM NOVO JOGO";
                            button2.Enabled = false;
                        }
                    }
                }
            }
        }

        private void DesabilitaBotoes()
        {
            foreach (Control controle in Controls)
            {
                if (controle.GetType().Name == "Button")
                {
                    controle.Enabled = false;
                } 
            } 
        }

        private void novoJogoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            qtdTurnos = 0;
            vezDoX = true;
            label6.Text = "0"; 
            label7.Text = "0";
            label8.Text = "0";

            foreach (Control controle in Controls)
            {
                if (controle.GetType().Name == "Button")
                {
                    controle.Enabled = true;
                    controle.Text = "";
                    button2.Text = "TROCAR SIMBOLO";
                    label1.Text = "X";
                    label2.Text = "O";
                }
            }
        }

        private void button_enter(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (vezDoX)
            {                
                    button.Text = "X";                               
            }
            else
            {              
                    button.Text = "O";                              
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Enabled)
            {
                button.Text = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();

            form2.ShowDialog();

            labelJogador1.Text = nomeJogadorUm;
            labelJogador2.Text = nomeJogadorDois;
            labelPlayer1.Text = nomeJogadorUm;
            labelPlayer2.Text = nomeJogadorDois;
        }

        private void ComputadorFazJogada()
        {
            Button jogada = null;

            jogada = ProcurePorVitoriaOuBloqueio(label2.Text);

            if (jogada == null)
            {
                jogada = ProcurePorVitoriaOuBloqueio(label1.Text);
                if (jogada == null)
                {
                    jogada = ProcuraPorCanto();
                    if (jogada == null)
                    {
                        jogada = ProcuraEspacoLivre();
                    }
                }
            }

            if (qtdTurnos < 9)
            {
                jogada.PerformClick();
            }
            
        }

        private Button ProcurePorVitoriaOuBloqueio(string simbolo)
        {
            //horizontais i.a
            if ((A1.Text == simbolo) && (A2.Text == simbolo) && (A3.Text == ""))
                return A3;
            if ((A1.Text == simbolo) && (A3.Text == simbolo) && (A2.Text == ""))
                return A2;
            if ((A3.Text == simbolo) && (A2.Text == simbolo) && (A1.Text == ""))
                return A1;

            if ((B1.Text == simbolo) && (B2.Text == simbolo) && (B3.Text == ""))
                return B3;
            if ((B1.Text == simbolo) && (B3.Text == simbolo) && (B2.Text == ""))
                return B2;
            if ((B3.Text == simbolo) && (B2.Text == simbolo) && (B1.Text == ""))
                return B1;

            if ((C1.Text == simbolo) && (C2.Text == simbolo) && (C3.Text == ""))
                return C3;
            if ((C1.Text == simbolo) && (C3.Text == simbolo) && (C2.Text == ""))
                return C2;
            if ((C3.Text == simbolo) && (C2.Text == simbolo) && (C1.Text == ""))
                return C1;

            //verticais i.a
            if ((A1.Text == simbolo) && (B1.Text == simbolo) && (C1.Text == ""))
                return C1;
            if ((A1.Text == simbolo) && (C1.Text == simbolo) && (B1.Text == ""))
                return B1;
            if ((C1.Text == simbolo) && (B1.Text == simbolo) && (A1.Text == ""))
                return A1;

            if ((A2.Text == simbolo) && (B2.Text == simbolo) && (C2.Text == ""))
                return C2;
            if ((B2.Text == simbolo) && (C2.Text == simbolo) && (A2.Text == ""))
                return B1;
            if ((A2.Text == simbolo) && (C2.Text == simbolo) && (B2.Text == ""))
                return A1;

            if ((A3.Text == simbolo) && (B3.Text == simbolo) && (C3.Text == ""))
                return C1;
            if ((A3.Text == simbolo) && (C3.Text == simbolo) && (B3.Text == ""))
                return B1;
            if ((C1.Text == simbolo) && (B1.Text == simbolo) && (A3.Text == ""))
                return A1;

            //verifica as diagonais
            if ((A1.Text == simbolo) && (B2.Text == simbolo) && (C3.Text == ""))
                return C3;
            if ((B2.Text == simbolo) && (C3.Text == simbolo) && (A1.Text == ""))
                return A1;
            if ((A1.Text == simbolo) && (C3.Text == simbolo) && (B2.Text == ""))
                return B2;

            if ((A3.Text == simbolo) && (B2.Text == simbolo) && (C1.Text == ""))
                return C1;
            if ((B2.Text == simbolo) && (C1.Text == simbolo) && (A3.Text == ""))
                return A3;
            if ((A3.Text == simbolo) && (C1.Text == simbolo) && (B2.Text == ""))
                return B2;

            return null;
        }

        private Button ProcuraPorCanto()
        {
            if (A1.Text == label2.Text)
            {
                if (A3.Text == "")
                    return A3;
                if (C3.Text == "")
                    return C3;
                if (C1.Text == "")
                    return C1;
            }

            if (A3.Text == label2.Text)
            {
                if (A1.Text == "")
                    return A1;
                if (C3.Text == "")
                    return C3;
                if (C1.Text == "")
                    return C1;
            }

            if (C3.Text == label2.Text)
            {
                if (A1.Text == "")
                    return A1;
                if (A3.Text == "")
                    return A3;
                if (C1.Text == "")
                    return C1;
            }
            if (C1.Text == label2.Text)
            {
                if (A1.Text == "")
                    return A1;
                if (A3.Text == "")
                    return A3;
                if (C3.Text == "")
                    return C3;
            }

            return null;
        }

        private Button ProcuraEspacoLivre()
        {
            Button button = null;
            foreach (Control control in Controls)
            {
                button = control as Button;
                if (button != null)
                {
                    if (button.Text == "")
                    {
                        return button;
                    }
                }
            }

            return null;
        }

        private void labelPlayer2_TextChanged(object sender, EventArgs e)
        {
            if (labelJogador2.Text.ToUpper() == "COMPUTADOR" || labelJogador2.Text.ToUpper() == "COMP")
            {
                contraOComputador = true;
            }
            else
            {
                contraOComputador = false;
            }
        }
    }
}
